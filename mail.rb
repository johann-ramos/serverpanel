#!/usr/bin/ruby
#
# Author: Johann Ramos
#
# Description: Script que recibe el correo y pass del login de la aplicacion para luego enviar un correo
# cuando se ejecuta algun boton de la gui
#
require 'net/smtp'

def mailadd(correo, pass)
    $correo = correo
    $pass = pass
end

def mailto(host, ip)
    send1 = "infraestructurab2b@bbr.cl"
    send2 = "ayudab2b@bbr.cl"
    host = host
    ip = ip
    time = Time.new
    smtp = Net::SMTP.new('smtp.bbr.cl', 25 )
mail = <<MESSAGE_END
From: BBR ServerPanel <#{$correo}>
To: infraestructurab2b <infraestructurab2b@bbr.cl>
MIME-Version: 1.0
Content-type: text/html
Subject: Reinicio de #{host} correo

<h1>Reinicio de sistema</h1>
<p style="font-size:large;">Se ha reiniciado <b>#{host}</b> de ip: #{ip}</p>
<p style="font-size:medium;">#{time.ctime}</p>
<p style="font-size:medium;">Atte. #{$correo}</p>
MESSAGE_END
    smtp.start('mail.bbr.cl', $correo, $pass, :login) do |smtp|
       smtp.send_message mail, $correo, [send1, send2]
       #smtp.send_message mail, $correo, userto2
    end

end