#!/usr/bin/ruby
#
# Author: Johann Ramos
#
# Description: Gui de la aplicacion donde grafican los botones de todos los ambientes, para agregar mas
# ambientes seguir la regla de stack -> para -> button
#
require 'rubygems'
require 'green_shoes'
require_relative 'sp'

class Gui
	def initialize(app)
		@app = app
	end
	def add
		@app.flow :widht => "100%" do
			# Stack Unimarc
			@app.stack :width => "10%" do
				@app.para "SMU"
				@app.button "Unimarc21" do
					puts ""
					puts "Server: unimarc21.smu.b2b"
					if confirm("Reiniciar AS?")
						sshcall("unimarc21.smu.b2b")
						alert "AS Reiniciado"
        			end
				end
				@app.button "Unimarc22" do
					puts ""
					puts "Server: unimarc22.smu.b2b"
					if confirm("Reiniciar AS?")
						sshcall("unimarc22.smu.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "Unimarc23" do
					puts ""
					puts "Server: unimarc23.smu.b2b"
					if confirm("Reiniciar AS?")
						sshcall("unimarc23.smu.b2b")
						alert "AS Reiniciado"
					end
				end
			end
			# Stack Oechsle
			@app.stack :width => "10%" do
				@app.para "Oechsle"
				@app.button "Lima" do
					puts ""
					puts "Server: lima.oechsle.b2b"
					if confirm("Reiniciar AS?")
						sshcall("lima.oechsle.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "Cusco" do
					puts ""
					puts "Server: cusco.oechsle.b2b"
					if confirm("Reiniciar AS?")
						sshcall("cusco.oechsle.b2b")
						alert "AS Reiniciado"
					end
				end
			end
			# Stack Paris
			@app.stack :width => "10%" do
				@app.para "Paris"
				@app.button "Lyon" do
					puts ""
					puts "Server: lyon.paris.b2b"
					if confirm("Reiniciar AS?")
						sshcall("lyon.paris.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "Lemans" do
					puts ""
					puts "Server: lemans.paris.b2b"
					if confirm("Reiniciar AS?")
						sshcall("lemans.paris.b2b")
						alert "AS Reiniciado"
					end
				end
			end
			# Stack SPSA
			@app.stack :width => "10%" do
				@app.para "SPSA"
				@app.button "Spsa21" do
					puts ""
					puts "Server: spsa21.ifhperu.b2b"
					if confirm("Reiniciar AS?")
						sshcall("spsa21.ifhperu.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "Spsa22" do
					puts ""
					puts "Server: spsa22.ifhperu.b2b"
					if confirm("Reiniciar AS?")
						sshcall("spsa22.ifhperu.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "Spsa23" do
					puts ""
					puts "Server: spsa23.ifhperu.b2b"
					if confirm("Reiniciar AS?")
						sshcall("spsa23.ifhperu.b2b")
						alert "AS Reiniciado"
					end
				end
			end

			# Stack SPSA Test
			@app.stack :width => "10%" do
				@app.para "SPSA Test"
				@app.button "Spsa21test" do
					puts ""
					puts "Server: spsa21test.ifhperu.b2b"
					if confirm("Reiniciar AS?")
						sshcall("spsa21test.ifhperu.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "Spsa22test" do
					puts ""
					puts "Server: spsa22test.ifhperu.b2b"
					if confirm("Reiniciar AS?")
						sshcall("spsa22test.ifhperu.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "Spsa23test" do
					puts ""
					puts "Server: spsa23test.ifhperu.b2b"
					if confirm("Reiniciar AS?")
						sshcall("spsa23test.ifhperu.b2b")
						alert "AS Reiniciado"
					end
				end
			end

			# Stack Promart
			@app.stack :width => "10%" do
				@app.para "Promart"
				@app.button "Arequipa" do
					puts ""
					puts "Server: arequipa.promart.b2b"
					if confirm("Reiniciar AS?")
						sshcall("arequipa.promart.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "Iquitos" do
					puts ""
					puts "Server: iquitos.promart.b2b"
					if confirm("Reiniciar AS?")
						sshcall("iquitos.promart.b2b")
						alert "AS Reiniciado"
					end
				end
			end

			# Stack SuperCL
			@app.stack :width => "10%" do
				@app.para "SuperCL"
				@app.button "SuperCL21" do
					puts ""
					puts "Server: supercl21.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("supercl21.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "SuperCL22" do
					puts ""
					puts "Server: supercl22.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("supercl22.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "SuperCL23" do
					puts ""
					puts "Server: supercl23.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("supercl23.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
			end

			# Stack EasyCL
			@app.stack :width => "10%" do
				@app.para "EasyCL"
				@app.button "EasyCL21" do
					puts ""
					puts "Server: easycl21.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("easycl21.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "EasyCL22" do
					puts ""
					puts "Server: easycl22.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("easycl22.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "EasyCL23" do
					puts ""
					puts "Server: easycl23.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("easycl23.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
			end

			# Stack SuperAR
			@app.stack :width => "10%" do
				@app.para "SuperAR"
				@app.button "SuperAR21" do
					puts ""
					puts "Server: superar21.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("superar21.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "SuperAR22" do
					puts ""
					puts "Server: superar22.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("superar22.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "SuperAR23" do
					puts ""
					puts "Server: superar23.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("superar23.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
			end
			
			# Stack EasyAR
			@app.stack :width => "10%" do
				@app.para "EasyAR"
				@app.button "EasyAR21" do
					puts ""
					puts "Server: easyar21.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("easyar21.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "EasyAR22" do
					puts ""
					puts "Server: easyar22.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("easyar22.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "EasyAR23" do
					puts ""
					puts "Server: easyar23.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("easyar23.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
			end

			# Stack EasyCO
			@app.stack :width => "10%" do
				@app.para "EasyCO"
				@app.button "EasyCO21" do
					puts ""
					puts "Server: easyco21.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("easyco21.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "EasyCO22" do
					puts ""
					puts "Server: easyco22.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("easyco22.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "EasyCO23" do
					puts ""
					puts "Server: easyco23.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("easyco23.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
			end

			# Stack ParisPE
			@app.stack :width => "10%" do
				@app.para "ParisPE"
				@app.button "ParisPE21" do
					puts ""
					puts "Server: parispe21.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("parispe21.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "ParisPE22" do
					puts ""
					puts "Server: parispe22.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("parispe22.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "ParisPE23" do
					puts ""
					puts "Server: parispe23.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("parispe23.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
			end

			# Stack SuperPE
			@app.stack :width => "10%" do
				@app.para "SuperPE"
				@app.button "SuperPE21" do
					puts ""
					puts "Server: superpe21.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("superpe21.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "SuperPE22" do
					puts ""
					puts "Server: superpe22.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("superpe22.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "SuperPE23" do
					puts ""
					puts "Server: superpe23.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("superpe23.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
			end

			# Stack Shopping
			@app.stack :width => "10%" do
				@app.para "Shopping"
				@app.button "Shopping21" do
					puts ""
					puts "Server: shopping21.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("shopping21.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "Shopping28" do
					puts ""
					puts "Server: shopping28.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("shopping28.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
			end

			# Stack SuperBR
			@app.stack :width => "10%" do
				@app.para "SuperBR"
				@app.button "SuperBR21" do
					puts ""
					puts "Server: superbr21.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("superbr21.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "SuperBR22" do
					puts ""
					puts "Server: superbr22.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("superbr22.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "SuperBR23" do
					puts ""
					puts "Server: superbr23.cencosud.b2b"
					if confirm("Reiniciar AS?")
						sshcall("superbr23.cencosud.b2b")
						alert "AS Reiniciado"
					end
				end
			end

			# Stack Corona
			@app.stack :width => "10%" do
				@app.para "Corona"
				@app.button "Corona21" do
					puts ""
					puts "Server: corona21.corona.b2b"
					if confirm("Reiniciar AS?")
						sshcall("corona21.corona.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "Corona22" do
					puts ""
					puts "Server: corona22.corona.b2b"
					if confirm("Reiniciar AS?")
						sshcall("corona22.corona.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "Corona23" do
					puts ""
					puts "Server: corona23.corona.b2b"
					if confirm("Reiniciar AS?")
						sshcall("corona23.corona.b2b")
						alert "AS Reiniciado"
					end
				end
			end

			# Stack Fasa
			@app.stack :width => "10%" do
				@app.para "Fasa"
				@app.button "Fasa21" do
					puts ""
					puts "Server: fasa21.fasa.b2b"
					if confirm("Reiniciar AS?")
						sshcall("fasa21.fasa.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "Fasa22" do
					puts ""
					puts "Server: fasa22.fasa.b2b"
					if confirm("Reiniciar AS?")
						sshcall("fasa22.fasa.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "Fasa23" do
					puts ""
					puts "Server: fasa23.fasa.b2b"
					if confirm("Reiniciar AS?")
						sshcall("fasa23.fasa.b2b")
						alert "AS Reiniciado"
					end
				end
			end

			# Stack Hites
			@app.stack :width => "10%" do
				@app.para "Hites"
				@app.button "doberman" do
					puts ""
					puts "Server: doberman.hites.b2b"
					if confirm("Reiniciar AS?")
						sshcall("doberman.hites.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "galgo" do
					puts ""
					puts "Server: galgo.hites.b2b"
					if confirm("Reiniciar AS?")
						sshcall("galgo.hites.b2b")
						alert "AS Reiniciado"
					end
				end
			end

			# Stack La Polar
			@app.stack :width => "10%" do
				@app.para "LaPolar"
				@app.button "capricornio" do
					puts ""
					puts "Server: capricornio.lapolar.b2b"
					if confirm("Reiniciar AS?")
						sshcall("capricornio.lapolar.b2b")
						alert "AS Reiniciado"
					end
				end
				@app.button "acuario" do
					puts ""
					puts "Server: acuario.lapolar.b2b"
					if confirm("Reiniciar AS?")
						sshcall("acuario.lapolar.b2b")
						alert "AS Reiniciado"
					end
				end
			end

		end
	end
end