#!/usr/bin/ruby
#
# Author: Johann Ramos
#
# Description: Script principal de ejecucion de la aplicacion.
#
require 'rubygems'
require 'green_shoes'
require_relative 'login'
require_relative 'gui'
require_relative 'mail'

Shoes.app title: "BBR RESTART SYSTEM", :width => 450, :height => 100 do
    flow do
        para "Usuario (correo bbr)               Password"
        @el1 = edit_line
        @el2 = edit_line secret:true
        @b1 = button "OK" do
            if @el1.text().to_s == ''
                alert "Ingrese su correo bbr"
            else
                if @el2.text().to_s == ''
                    alert "Ingrese su password"
                else
                    if valid(@el1.text(), @el2.text()) == true
                        @mail = @el1.text()
                        @pass = @el2.text()
                        mailadd(@mail, @pass)
                        # APP
                        Shoes.app title: "BBR RESTART SYSTEM", :width => 900, :height => 250 do
                            svp = Gui.new(self) # passing in the app here
                            svp.add
                        end
                        close()
                    else
                        alert "usuario o password incorrectos"
                    end
                end
            end
        end
    end
end