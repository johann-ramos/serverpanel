#!/usr/bin/ruby
#
# Author: Johann Ramos
#
# Description: Script que valida que la cuenta de usuario sea valida, solo puede ingresar los usuarios que
# tengan un correo bbr valido.
#
require 'net/smtp'

def valid(user, pass)
    @user = user
    @pass = pass
    result = ""
    #Using Block
    smtp = Net::SMTP.new('smtp.bbr.cl', 25 )
    #smtp.enable_starttls
    begin
        smtp.start('mail.bbr.cl', user, pass, :login) do |smtp|
        return true
    end
    rescue Exception => e
        return false
    end
end