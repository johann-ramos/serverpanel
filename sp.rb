#!/usr/bin/ruby
#
# Author: Johann Ramos
#
# Description: Script que se encarga de ejecutar un comando en cada uno de los servidores.
# Para cambiar el comando a ejecutar en cada servidor editar la variable comm.
# Si la llamada al comando jRestart.sh o wRestart.sh no funciona verificar que este script exista en el servidor.
#
require 'net/ssh'
require_relative 'mail.rb'
require_relative 'login.rb'

def sshcall(host)
	host = host
	user = ""
	pass = ""
	if host.include? "paris.b2b" or host.include? "lapolar.b2b"
		puts "AS: Webshpere"
		comm = "sh wRestart.sh"
	else
		puts "AS: JBoss"
		comm = "sh jRestart.sh"
	end

	begin
		#comm = "hostname && ls -l"
		ip = "ip addr | grep eth | grep inet | awk '{print $2}' | cut -f1 -d'/'"
		Net::SSH.start(host, user, :password => pass, :timeout => 15) do |ssh|
		# capture all stderr and stdout output from a remote process
		output = ssh.exec!("#{comm}")
		opip = ssh.exec!("#{ip}")
		puts output
		mailto(host,opip)
		puts "Reinicio completado"
	end
	rescue Timeout::Error => e
		puts "  Timed out"
	rescue Errno::EHOSTUNREACH => e
		puts "  Host unreachable"
	rescue Errno::ECONNREFUSED
		puts "  Connection refused"
	rescue Net::SSH::AuthenticationFailed
		puts "  Authentication failure"
	end
end
